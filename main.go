package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/calculate", DefaultHandler)

	srv := &http.Server{
		Handler: r,
		Addr:    "127.0.0.1:1000",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	fmt.Println("Server started at http://" + srv.Addr)
	log.Fatal(srv.ListenAndServe())
}

func DefaultHandler(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	var result string
	if query.Get("operator") == "+" {
		operands := query["operands"]
		var sum int
		for i, operand := range operands {
			number, err := strconv.Atoi(operand)
			if err != nil {
				returnJson("", 400, "Invalid value of operands, please give number value", w)
				return
			}
			if i == 0 {
				sum = number
				continue
			}
			sum += number
		}
		result = strconv.Itoa(sum)
	} else if query.Get("operator") == "-" {
		operands := query["operands"]
		var sum int
		for i, operand := range operands {
			number, err := strconv.Atoi(operand)
			if err != nil {
				returnJson("", 400, "Invalid value of operands, please give number value", w)
				return
			}
			if i == 0 {
				sum = number
				continue
			}
			sum -= number
			result = strconv.Itoa(sum)
		}
	} else if query.Get("operator") == "*" {
		operands := query["operands"]
		var sum int
		for i, operand := range operands {
			number, err := strconv.Atoi(operand)
			if err != nil {
				returnJson("", 400, "Invalid value of operands, please give number value", w)
				return
			}
			if i == 0 {
				sum = number
				continue
			}
			sum *= number
			result = strconv.Itoa(sum)
		}
	} else if query.Get("operator") == "/" {
		operands := query["operands"]
		var sum float64
		for i, operand := range operands {
			number, err := strconv.ParseFloat(operand, 32)
			if err != nil {
				returnJson("", 400, "Invalid value of operands, please give number value", w)
				return
			}
			if i == 0 {
				sum = number
				continue
			}
			sum /= number
			result = strconv.FormatFloat(sum, 'f', 3, 64)
		}
	} else {
		returnJson(result, 400, "Invalid Operator, please only use + - * /", w)
		return
	}

	returnJson(result, 200, "success", w)
}

func returnJson(result string, statusCode int, message string, w http.ResponseWriter) {
	data := map[string]interface{}{
		"code":    statusCode,
		"message": message,
		"result":  result,
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if statusCode == 200 {
		w.WriteHeader(http.StatusOK)
	} else if statusCode == 400 {
		w.WriteHeader(http.StatusBadRequest)

	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}
